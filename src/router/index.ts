import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import itemList from '@/views/Item-list.vue';

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{  // 동적 세그먼트는 콜론으로 시작합니다.
    path: '/:status?',
    name: 'item-list',
    component: itemList,
  }]
})

export default router
